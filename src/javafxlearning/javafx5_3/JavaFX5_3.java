/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx5_3;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class JavaFX5_3 extends Application
{

    @Override
    public void start(Stage primaryStage)
    {
        VBox root = new VBox();

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();

        AnimationTimer timer = new AnimationTimer()
        {
            private int frame = 0;
            private long firstNow = 0;

            @Override
            public void handle(long now)
            {
                if (frame == 0)
                {
                    firstNow = now;
                }
                if (frame++ % 60 == 0)
                {
                    Label fLabel = new Label(""+frame/60 + ":");
                    Label nowLabel = new Label(""+(now-firstNow)/10000000/100.0);
                    HBox hbox = new HBox(fLabel, nowLabel);
                    hbox.setSpacing(30);
                    root.getChildren().add(hbox);
                }
            }
        };
        timer.start();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
