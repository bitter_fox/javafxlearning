/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx6_1;

import java.util.Arrays;
import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.Line;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author bitter_fox
 */
public class JavaFX6_1 extends Application
{

    @Override
    public void start(Stage primaryStage)
    {
        Rectangle r = new Rectangle(30, 30, 200, 100);
        r.setFill(Color.AQUA);
        r.setStroke(Color.DARKBLUE);

        Circle c = new Circle(400, 80, 50);
        c.setFill(Color.RED);
        c.setStroke(Color.DARKRED);

        Line l = new Line(30, 200, 300, 330);

        Path p = new Path(
            new MoveTo(400, 200),
            new CubicCurveTo(400, 300, 700, 300, 700, 200),
            new HLineTo(400),
            new ClosePath()
        );
        p.setFill(Color.YELLOWGREEN);
        p.setStroke(Color.DARKGREEN);

        Button button = new Button("start animation");
        button.setOnAction(
            e ->
            {
                RotateTransition[] rotateTransitions =
                {
                    new RotateTransition(Duration.seconds(1), r),
                    new RotateTransition(Duration.seconds(1), c),
                    new RotateTransition(Duration.seconds(1), l),
                    new RotateTransition(Duration.seconds(1), p)
                };

                Arrays.stream(rotateTransitions)
                    .peek(t -> t.setFromAngle(0))
                    .peek(t -> t.setByAngle(360))
                    .forEach(RotateTransition::playFromStart);
            });

        Group root = new Group(r, c, l, p, button);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
