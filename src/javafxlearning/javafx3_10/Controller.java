/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx3_10;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author bitter_fox
 */
public class Controller
{
    @FXML
    private Label width;
    @FXML
    private Label height;

    static
    {
        System.out.println("Hello");
    }
    /**
     * Initializes the controller class.
     */
    @FXML
    private void initialize()
    {
        width.sceneProperty().addListener((o, ov, nv) -> this.initializeWithScene(nv));
    }

    private void initializeWithScene(Scene scene)
    {
        width.textProperty().bind(scene.widthProperty().asString());
        height.textProperty().bind(scene.heightProperty().asString());
    }
}
