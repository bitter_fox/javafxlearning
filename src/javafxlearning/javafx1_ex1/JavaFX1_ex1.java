/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx1_ex1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class JavaFX1_ex1 extends Application
{

    @Override
    public void start(Stage primaryStage)
    {
        Label label = new Label("Name: ");
        TextField textField = new TextField();
        Button button = new Button("Hello!");

        HBox root = new HBox();
        root.getChildren().add(label);
        root.getChildren().add(textField);
        root.getChildren().add(button);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
