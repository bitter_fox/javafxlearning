/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx2_2;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 *
 * @author bitter_fox
 */
public class Controller
{
    @FXML
    private Label label;

    @FXML
    private void onClickButton(ActionEvent event)
    {
        label.setText("HelloWorld!");
    }
}
