/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx6_2;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class JavaFX6_2 extends Application
{

    @Override
    public void start(Stage primaryStage)
    {
        Canvas canvas = new Canvas(700, 1000);
        Group root = new Group(canvas);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setFill(Color.AQUA);
        gc.setStroke(Color.DARKBLUE);
        gc.fillRect(30, 30, 200, 100);
        gc.strokeRect(30, 30, 200, 100);

        gc.setFill(Color.RED);
        gc.setStroke(Color.DARKRED);
        gc.fillOval(350, 30, 100, 100);
        gc.strokeOval(350, 30, 100, 100);

        gc.strokeLine(30, 200, 300, 330);

        gc.setFill(Color.YELLOWGREEN);
        gc.setStroke(Color.DARKGREEN);
        gc.beginPath();
        gc.moveTo(400, 200);
        gc.bezierCurveTo(400, 300, 700, 300, 700, 200);
        gc.lineTo(400, 200);
        gc.fill();
        gc.stroke();
        gc.closePath();

        gc.drawImage(new Image("http://upload.wikimedia.org/wikipedia/commons/4/45/Duke3D.png"), 30, 400, 200, 200);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
