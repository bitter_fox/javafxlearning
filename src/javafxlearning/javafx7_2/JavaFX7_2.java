/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx7_2;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.geometry.Point3D;
import javafx.scene.AmbientLight;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author Shinya
 */
public class JavaFX7_2 extends Application {

    @Override
    public void start(Stage primaryStage) {
        Sphere sphere = new Sphere(300);
        sphere.setTranslateX(500);
        sphere.setTranslateY(500);
        sphere.setRotationAxis(new Point3D(0, 1, 0));

        PhongMaterial material = new PhongMaterial();
        Image map = new Image("http://www.abysse.co.jp/world/map/images/miller_asi_on.gif");
        material.setDiffuseMap(map);
        sphere.setMaterial(material);

        Group root = new Group();
        root.getChildren().add(sphere);

        PointLight light = new PointLight();
        light.setTranslateX(800);
        light.setTranslateY(200);
        light.setTranslateZ(-200);
        root.getChildren().add(light);

        AmbientLight ambientLight = new AmbientLight(Color.rgb(255, 255, 255, 0.1));
        root.getChildren().add(ambientLight);

        Scene scene = new Scene(root, 300, 250);
        Camera camera = new PerspectiveCamera();
        scene.setCamera(camera);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();

        RotateTransition rotateTransition = new RotateTransition(Duration.seconds(10), sphere);
        rotateTransition.setByAngle(360);
        rotateTransition.setInterpolator(Interpolator.LINEAR);
        rotateTransition.setCycleCount(Animation.INDEFINITE);
        rotateTransition.playFromStart();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
