/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx3_1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author bitter_fox
 */
public class Controller implements Initializable
{
    @FXML
    private TextField textField;
    @FXML
    private Label label;
    @FXML
    private Label error;
    @FXML
    private Button toNext;

    private BooleanBinding validateLengthProperty;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        label.textProperty().bind(textField.textProperty()); // label.text = textField.text
        validateLengthProperty = textField.textProperty().length().lessThanOrEqualTo(10);
        error.textProperty().bind(
            Bindings.when(validateLengthProperty)
                .then("")
                .otherwise("名前は10文字以下で入力してください"));
        toNext.disableProperty().bind(validateLengthProperty.not());
    }
}
