/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx1_2;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class JavaFX1_2 extends Application
{

    @Override
    public void start(Stage primaryStage)                                       // Stage
    {
        Button btn = new Button();                                              // Button
        btn.setText("Say 'Hello World'");

        // Events
        btn.setOnAction(e -> System.out.println("Hello World!"));
        btn.setOnMouseEntered(e -> btn.setText("Click to Say"));
        btn.setOnMouseExited(e -> btn.setText("Say 'Hello World'"));

        StackPane root = new StackPane();                                       // StackPane
        root.getChildren().add(btn);                                            // StackPane - Button

        Scene scene = new Scene(root, 300, 250);                                // Scene - StackPane - Button

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);                                           // Stage - Scene - StackPane - Button
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
