/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx5_0;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;

/**
 * FXML Controller class
 *
 * @author bitter_fox
 */
public class Controller implements Initializable
{
    @FXML
    private Button button;
    @FXML
    private Slider translateXSlider;
    @FXML
    private Slider translateYSlider;
    @FXML
    private Slider scaleXSlider;
    @FXML
    private Slider scaleYSlider;
    @FXML
    private Slider rotateSlider;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        button.sceneProperty().addListener((o, ov, nv) -> initializeWithScene(nv));

        button.translateXProperty().bindBidirectional(translateXSlider.valueProperty());
        button.translateYProperty().bindBidirectional(translateYSlider.valueProperty());
        scaleXSlider.valueProperty().bindBidirectional(button.scaleXProperty());
        scaleYSlider.valueProperty().bindBidirectional(button.scaleYProperty());
        button.rotateProperty().bindBidirectional(rotateSlider.valueProperty());
    }

    private void initializeWithScene(Scene scene)
    {
        if (scene != null)
        {
            translateXSlider.minProperty().bind(scene.widthProperty().divide(2).negate());
            translateXSlider.maxProperty().bind(scene.widthProperty().divide(2));

            translateYSlider.minProperty().bind(scene.heightProperty().divide(2).negate());
            translateYSlider.maxProperty().bind(scene.heightProperty().divide(2));
        }
    }

}
