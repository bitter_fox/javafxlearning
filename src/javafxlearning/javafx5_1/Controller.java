/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx5_1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author bitter_fox
 */
public class Controller implements Initializable
{
    @FXML
    private Circle circle;
    @FXML
    private Rectangle rectangle;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
    }

    public void onClickButton()
    {
        ScaleTransition scale = new ScaleTransition(Duration.seconds(2), circle);
        scale.setFromX(1);
        scale.setByX(2);
        scale.setAutoReverse(true);
        scale.setCycleCount(2); // auto reverseの時は2以上，行きが1回帰りも1回として数えられる

        TranslateTransition translate = new TranslateTransition(Duration.seconds(3));
        translate.setFromX(0);
        translate.setFromY(0);
        translate.setByX(400);

        RotateTransition rotate = new RotateTransition(Duration.seconds(3));
        rotate.setFromAngle(0);
        rotate.setByAngle(360*3);

        ParallelTransition rectangleTransition = new ParallelTransition(rectangle, translate, rotate);

        scale.playFromStart();
        rectangleTransition.playFromStart();
    }
}
