/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx7_1;

import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Point3D;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author bitter_fox
 */
public class JavaFX7_1 extends Application
{

    @Override
    public void start(Stage primaryStage)
    {
        Group root = new Group();

        Box box = new Box(20, 20, 20);
        /*box.setTranslateX(50);
        box.setTranslateY(50);
        box.setTranslateZ(50);*/
        Point3D p = new Point3D(0, 1, 0);
        box.setRotationAxis(p);
        box.setRotate(30);

        PhongMaterial duke = new PhongMaterial();
        Image diffuseMap = new Image("http://wiki.bcmoney-mobiletv.com/images/1/10/Duke.jpg");
        duke.setDiffuseMap(diffuseMap);
        box.setMaterial(duke);

        root.getChildren().add(box);

        Scene scene = new Scene(root, 300, 250);
        Camera camera = new PerspectiveCamera(true);
        scene.setCamera(camera);
        camera.setTranslateZ(-100);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();

        RotateTransition rotateTransition = new RotateTransition(Duration.seconds(2), box);
        rotateTransition.setCycleCount(Animation.INDEFINITE);
        rotateTransition.setInterpolator(Interpolator.LINEAR);
        rotateTransition.setByAngle(360);
        rotateTransition.playFromStart();
        AnimationTimer an = new AnimationTimer() {
            private double x = 0, y = 1, z = 0;

            @Override
            public void handle(long l) {
                box.setRotationAxis(new Point3D(x, y, z));
                x+=0.1;
                z+=0.1;
            }
        };
        an.start();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
