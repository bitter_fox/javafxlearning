/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx5_2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author bitter_fox
 */
public class Controller implements Initializable
{
    @FXML
    private Circle circle;
    @FXML
    private Rectangle rectangle;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
    }

    public void onClickButton()
    {
        ScaleTransition scale = new ScaleTransition(Duration.seconds(5), circle);
        scale.setFromX(1);
        scale.setByX(2);

        Rotate r = new Rotate(0, 0, 0);
        rectangle.getTransforms().add(r);
        // 3秒間で，circle.tracnslateXの値を400にする
        Timeline tl = new Timeline(
            new KeyFrame(Duration.ZERO, new KeyValue(rectangle.translateXProperty(), 0)),
            new KeyFrame(Duration.seconds(3), new KeyValue(rectangle.translateXProperty(), 400),
                                              new KeyValue(r.angleProperty(), 360*3))
        );

        scale.playFromStart();
        tl.playFromStart();
    }
}
