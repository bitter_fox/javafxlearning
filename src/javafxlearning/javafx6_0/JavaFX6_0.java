/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx6_0;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class JavaFX6_0 extends Application
{

    @Override
    public void start(Stage primaryStage)
    {
        Image image = new Image("http://upload.wikimedia.org/wikipedia/commons/4/45/Duke3D.png");
        ImageView iv = new ImageView(image);
        iv.setFitWidth(300);
        iv.setFitHeight(300);

        Media media = new Media("http://download.oracle.com/otndocs/products/javafx/oow2010-2.flv");
        MediaPlayer mp = new MediaPlayer(media);
        MediaView mv = new MediaView(mp);

        VBox root = new VBox();
        root.getChildren().add(iv);
        root.getChildren().add(mv);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();

        mp.play();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
