/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx3_2;

import java.net.URL;
import java.util.OptionalDouble;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.binding.NumberBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author bitter_fox
 */
public class Controller implements Initializable
{
    @FXML
    private TextField height;
    @FXML
    private TextField weight;

    @FXML
    private Label bmi;
    private NumberBinding bmiProperty;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        IntegerBinding heightBinding = Bindings.createIntegerBinding(() -> parseInt(height.getText()), height.textProperty());
        IntegerBinding weightBinding = Bindings.createIntegerBinding(() -> parseInt(weight.getText()), weight.textProperty());
        DoubleBinding h = heightBinding.divide(100.0);
        bmiProperty = weightBinding.divide(h).divide(h);
        bmi.textProperty().bind(Bindings.when(bmiProperty.greaterThan(0)).then(bmiProperty.asString()).otherwise("BMI"));
    }

    private int parseInt(String str)
    {
        try
        {
            return Integer.parseInt(str);
        }
        catch (NumberFormatException nfe)
        {
            return 0;
        }
    }
}
