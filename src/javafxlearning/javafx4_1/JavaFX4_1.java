/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx4_1;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class JavaFX4_1 extends Application
{

    @Override
    public void start(Stage primaryStage) throws IOException                                       // Stage
    {
        Parent root = FXMLLoader.load(this.getClass().getResource("JavaFX4_1.fxml"));
        Scene scene = new Scene(root, 300, 250);                                // Scene - StackPane - Button

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);                                           // Stage - Scene - StackPane - Button
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
