/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxlearning.javafx4_1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.effect.MotionBlur;
import javafx.scene.effect.Reflection;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author bitter_fox
 */
public class Controller implements Initializable
{
    @FXML
    private Text txt1;
    @FXML
    private Text txt2;
    @FXML
    private Text txt3;
    @FXML
    private Text txt4;
    @FXML
    private Text txt5;
    @FXML
    private Text txt6;
    @FXML
    private Text txt7;
    @FXML
    private Text txt8;
    @FXML
    private Text txt9;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        Bloom bloom = new Bloom(0.1);
        Glow glow = new Glow(2);
        DropShadow dropShadow = new DropShadow(3, 20, 10, Color.DARKGRAY);
        GaussianBlur gaussianBlur = new GaussianBlur(3);
        Light.Distant distant = new Light.Distant();
        distant.setAzimuth(45);
        Lighting lighting = new Lighting(distant);
        MotionBlur motionBlur = new MotionBlur(0, 3);
        Reflection reflection = new Reflection(3, 1, 0.8, 0.2);

        Text[] texts = {txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9};
        Effect[] effects = {bloom, glow, dropShadow, gaussianBlur, lighting, motionBlur, reflection};

        for (int i = 0; i < effects.length; i++)
        {
            texts[i].setText(effects[i].getClass().getName());
            texts[i].setEffect(effects[i]);
        }
    }

}
